#include <iostream>
#include <cmath>

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(float _x, float _y, float _z): x(_x), y(_y), z(_z)
    {}

    void ShowV()
    {
        std::cout << x << ' ' << y << ' ' << z << std::endl;
    }
    float VectorLenght()
    {
        return sqrt(x * x + y * y + z * z);
    }

private:
    float x, y, z;
};

int main()
{
    Vector v(10, 12, 11);
    v.ShowV();
    std::cout << v.VectorLenght() << std::endl;
}